import { Component } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

// const now = new Date();
// const month = new Date();


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'app';
    public selectedDateRange = {name: '', value: '', current: false};
    public dateRanges = [
        { name: 'Today', value: 'day', current: true},
        { name: 'Yesterday', value: 'day', current: false},
        { name: 'This Week', value: 'week', current: true},
        { name: 'Last Week', value: 'week', current: false},
        { name: 'This Month', value: 'month', current: true},
        { name: 'Last Month', value: 'month', current: false},
        { name: 'This Quarter', value: 'quarter', current: true},
        { name: 'Last Quarter', value: 'quarter', current: false},
        { name: 'This Year', value: 'year', current: true},
        { name: 'Last Year', value: 'year', current: false},
    ];
    fromDate: NgbDateStruct;
    toDate: NgbDateStruct;

    constructor() {}

    public clearDates() {
        this.fromDate = null;
        this.toDate = null;
        this.selectedDateRange = {name: '', value: '', current: false};
    }

    public onDateChange($event) {
        console.log('Date changed');
    }

    public onDateSelected(date) {
        this.selectedDateRange = date;
        const startDate = moment();
        const endDate = moment();

        if (date.value === 'day') {
            const subtractor = date.current ? 0 : 1; // Today or yesterday?
            this.fromDate = {
                year: startDate.get('year'),
                month: startDate.get('month') + 1,
                day: startDate.get('date') - subtractor
            };
            this.toDate = {
                year: startDate.get('year'),
                month: startDate.get('month') + 1,
                day: startDate.get('date') - subtractor
            };
        } else {
            if (date.current) {
                startDate.startOf(date.value);
                endDate.endOf(date.value);
            } else {
                startDate.subtract(1, date.value).startOf(date.value);
                endDate.subtract(1, date.value).endOf(date.value);
            }
            this.fromDate = {
                year: startDate.get('year'),
                month: startDate.get('month') + 1,
                day: startDate.get('date')
            };
            this.toDate = {
                year: endDate.get('year'),
                month: endDate.get('month') + 1,
                day: endDate.get('date')
            };
        }
    }

    public onFromDateChange(date: NgbDateStruct) {}

    public onToDateChange(date: NgbDateStruct) {}
}
